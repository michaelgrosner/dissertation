\documentclass[letterpaper,11pt]{article}
\usepackage[left=1.5in,top=1in,right=1in,bottom=1in]{geometry}
\linespread{2.0}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{cite}
\everymath{\displaystyle}

\usepackage{sectsty}
\sectionfont{\normalsize}
\subsectionfont{\small}
\usepackage[compact]{titlesec}

\title{Investigating the Complex Looping Behavior of the Lac Operon Through Monte-Carlo Simulation: Exam B Qualifier}
\date{\today}
\author{Michael Grosner}

\begin{document}

\maketitle

\section{Introduction}
\subsection{DNA}
% What is DNA?
The polymeric molecule deoxyribonucleic acid (DNA) encodes and stores the genetic instructions used by all living organisms. The genetic code within the DNA of every living cell is read and duplicated, providing the organism to respond to its environment and reproduce. The genetic code, once read, is transcribed into RNA, which is converted into proteins which carry out the many functions of life. 

% What are base pairs?
The most fundamental building block of DNA and other nucleic acids are the nucleobases. The four most common bases in DNA are adenine (A), guanine (G), cytosine (C), and thymine (T).  Other important nucleic acids exist but are not commonly found in DNA, such as Uracil (U). Cytosine and thymine are singly aromatic-ringed pyrimidines and adenine and guanine are double rings, consisting of a pyramidine ring fused to a imidazole ring. The "genetic code" is a mapping of 3 successive nucleic acids into 1 of the 20 amino acids, the fundamental building blocks of proteins.  

% How are DNA nucelotides combined into base pairs?
These four bases form hydrogen bonds according to the Watson-Crick pairings, termed base pairs. Each base pair consists of 2 hydrogen bonded nucleotide bases, usually as the Watson-Crick pairings A-T and C-G. These base pairings form long, helical heteropolymeric chains via the base-stacking interactions of successive aromatic rings.

\subsection{DNA Looping}
For a long time (how long? citations?), it was assumed that information flowed only in this one direction away from DNA and towards proteins. In reality, the chaotic physical interactions of DNA in its tightly packed nucleus between itself and proteins, can block off important biologically important regions and affect the transcription and translational process. 

Organisms take advantage of this tangled chaos by coordinating the production and binding of DNA bending factors capable of blocking off these regions. These factors serve as a finely tuned genetic switch in response to environmental stimuli. DNA bending plays a critical role in the stability of the nucleosome, p53, DNA Topoisomerase I, chromatin remodeling factors, and RXR \cite{Virstedt2004, Garcia2007}. The modeling and analysis methods contained within this dissertation may also be applied to these systems.

\subsection{The lac Operon}
% What is it's history?
The most famous and best studied example of this genetic control is the Lac operon in \textit{Escherichia coli}, which codes for proteins assisting in the breakdown of lactose. First discovered by Jacob and Monod. 

% What does the lac operon do? What is interesting about it?
%% Zoomed out structure
An operon is a genomic region under the control of an external regulatory mechanism. In a promoter, a regulatory protein, called a repressor, binds to the operator gene to prevent RNA polymerase from binding to the promoter region and preventing the transcription of RNA. In the lac Operon, there are 3 distinct operator regions participating in repression. The regulated, protein-encoding regions downstream of the promoter and operators encode $\beta$-galactosidase, lactose permease, and thiogalactoside transacetylase proteins. The $\beta$-galactosidase and lactose permease proteins are instrumental in digesting and transporting lactose in E. coli, respectively. Remarkably, when lactose is not present in the E. coli's environment or when it's preferred food source glucose is available, the cell is able to switch off production of these three genes via the Lac Repressor protein, a protein encoded just upstream of the lac operon as diagrammed in Figure \ref{fig:LacOperonZoomedOut}.

\begin{figure}[h]
\centering
\includegraphics[scale=.8]{LacOperonZoomedOut.png}
\caption{\small The lac operon consists of a promoter region (P lac), three operator regions (O1, O2, O3), regulated genes (Z, Y, A) and repressor protein (i) with it's own promoter and gene region. When repressor protein LacR is present in the nucleus of E. coli and binds to the operator region, production of the ZYA genes cease. Image taken from \cite{Muller-Hill1996} }
\label{fig:LacOperonZoomedOut}
\end{figure} 

%% Operators and looped region
The Lac repressor protein (LacR) binds to two distant regions in the Lac operon simultaneously, at sites called operators, forming a loop, shown in Figures \ref{fig:LacOperonStructure} and \ref{fig:LacOperonStructureWithHU} \cite{Kraemer1987}. This action prevents RNA polymerase from binding at the Lac promoter region and represses production of the Lac genes. Interestingly, some of these Lac operon loops form with a much higher probability and the levels of repression are greater than theoretical models of DNA stiffness predict given the typical distance of looped DNA between operators. 

\begin{figure}[h]
\centering
\includegraphics[scale=1]{LacOperonZoomedIn.png}
\caption{\small The Lac operon consists of three operators. Image taken from \cite{Muller-Hill1996} }
\label{fig:LacOperonZoomedIn}
\end{figure} 

\begin{figure}[h]
\centering
\includegraphics[scale=.7]{A2_109bp_-HU.png}
\caption{\small The energy-optimized structure of the Lac repressor (purple) bound to two $O_{sym}$ operators between 95bp of free DNA in an A2 orientation. LacR-bound loops are conventionally measured midpoint of first operator to midpoint of second operator, a total of 14 base pairs from LacR, making this a 109bp loop. Image taken from \cite{Olson2012}.}
\label{fig:LacOperonStructure}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=3]{P2flex_115bp_2HU.png}
\caption{\small The energy-optimized structure of a P2, 115bp Lac repressor mediated loop bound to two $O_{sym}$ operators between 101bp of free DNA and two bound HU (yellow). HU often binds near the operators for P2 loops of 92 and 115bp length. The LacR protein here has deformed itself to accommodate the stress built up at the apex of the loop. Image taken from \cite{Olson2012}. }
\label{fig:LacOperonStructureWithHU}
\end{figure}

\subsection{Architectural Proteins}
% What are architectural proteins? Examples?
The lac operon system exists within in the larger and much more complex nucleus system. Proteins other than LacR and RNA Polymerase may also bind and deform DNA at any time. The Architectural Proteins are a class of nucleoid proteins, and may bind without strong sequence specificity and sharply bend DNA. In E. coli, some examples of these proteins include HU, I-HF, Fis and Hfq. These proteins are abundantly present in the growth phase of E coli; for example about 30,000-50,000 copies of each of Fis, Hfq, and HU may be present, or about one copy for every 150bp of DNA \cite{AliAzam1999, Ditto1994}.

% What is HU?
The histone-like heat unstable architectural protein HU is a small (19 kDa), dimeric protein responsible for forming some of the sharpest known kinks in bacterial DNA. These kinks form with little sequence specificity, and the protein preferentially binds into the minor groove of the DNA. The 4 x-ray structures of HU - PDB IDs 1P71, 1P78, and 1P51a+b - exhibit bound DNA bent by about 105 to 140 degrees \cite{Swinger2003} and unwind the double helix of the DNA. HU contains a hydrophobic core supporting two $\beta$-ribbon arms which kink DNA twice, separated by 9bp \cite{Swinger2007, Sagi2004}.

The absence of HU in E coli has been shown via gene repression studies to significantly inhibit the ability of the cell to repress production of the lac genes \cite{Becker2005, Becker2007, Bond2010}. In addition, the presence of HU lowers the minimum DNA chain length needed for ligated ring closure \cite{Hodges-Garcia1989}. Similar to the nucleosome, HU participates in genomic compaction at certain concentrations, acting to prevent DNA from adopting it's stiff, straight relaxed state \cite{Rouviere-Yaniv1979}. Therefore, HU seems intimately required in reducing the energy needed for forming repressing lac loops.
% negative writhe

\begin{figure}[h]
\centering
\includegraphics[scale=.5]{HU_structure.png}
\caption{\small The architectural protein HU (PDB ID 1P78) exhibiting the strongly bent angle of bound DNA.}
\label{fig:HU_structure}
\end{figure}

\subsection{Differences between the observed in vivo and in vitro flexibility of DNA}

The main goals of my doctoral studies will be to improve the biological picture of DNA looping via a model system of the Lac operon, and to bridge the gap between physical models of DNA and the results of \textit{in vivo} experimentation. To investigate this unsolved problem, I already have studied the effects of varying the chain length of looped DNA, adding architectural proteins, or nucleoid proteins capable of deforming the DNA with a low degree of sequence specificity, different operator types and structures, and allowing the LacR protein to deform itself. In addition, I propose enhancing the DNA model used in simulation to include sequence-dependent effects and investigate the formation of sharp kinks observed in DNA.

\begin{figure}[h]
\centering
\includegraphics[scale=.7]{step_par.png}
\caption{\small Schematic diagram of the six step parameters which fully describe the transformation of one base pair to the next. Image from \cite{Olson1998}, derived from \cite{Dickerson1989}.}
\label{fig:StepParams}
\end{figure}

\section{Mechanics of DNA Looping}
Treating DNA base pairs as a series of discrete, rigid, rectangular planes affords a computationally tractable formalism and can capture most of the physical behavior of the system. 

The most fundamental building block of nucleic acids are the nucleotide bases. The four most common bases in DNA, adenine (A), guanine (G), cytosine (C), and thymine (T), contain aromatic rings which give the nucloside portion a relatively planar shape. 

These four bases form hydrogen bonds according to the Watson-Crick pairings, termed base pairs. Each base pair consists of 2 hydrogen bonded nucleotide bases, usually as the Watson-Crick pairings A-T and C-G. Base pairs are approximated as rigid, rectangular planes. 

Successive base pairs constitute a base-pair step. Six step parameters express the transformation from one base pair to the next, are called tilt, roll, twist, shift, slide, rise (see Figure \ref{fig:StepParams}) \cite{Dickerson1989,ElHassan1995} are often represented as the length-6 vector $\theta$. The tilt, roll, and twist parameters are measured in degrees as angles, and shift, slide, and rise are measured in angstroms as Cartesian distances. Since steps are described in the displacement of one step to the next, the 5' to 3' directionality of DNA is preserved along the edges. 

Single base pairings often deviate from an idealized single plane. Bonded bases may be sheared, stretched, staggered, buckled, opened, or propellered from its pair. These measurements are not considered in this simplified model, and will only deal with the parameters describing the translation from base pair to base pair. In addition, the entire phosphate backbone is disregarded in this model. 

A harmonic potential energy function, derived from the co-variance of the six step parameters observed in crystal structures \cite{Olson1998}, can model the interactions between successive base pair steps,

\begin{align}
\Psi &= E_{0} + \frac{1}{2} \sum_{i=1}^6 \sum_{j=1}^6 f_{ij} \Delta \theta_i \Delta \theta_j = E_{0} +  \frac{1}{2} \Delta x^T F \Delta x
\label{eq:harmonic-energy}
\end{align}

In equation \ref{eq:harmonic-energy}, $E_0$ is the rest energy. Since this energy is not measured, we set $E_0 = 0$, and therefore the energy computations are all relative to this zero, relaxed energy state. 

$\Delta \theta_i$ and $\Delta \theta_j$ are the fluctuations from the set of intrinsic values.

$F$ is a 6x6 matrix of stiffness constants where values should be chosen such that the resulting DNA has a persistence length, a key property of the flexibility of a polymer, of about $500 \AA$. The diagonal elements $f_{ii}$ are the elastic force constants while the off diagonal elements describe the coupling between parameters.

For ideal and naturally straight B-DNA, the intrinsic values of $\theta$ are all zeros except for $\theta_{twist} = \frac{360}{10.5}$ degrees, $\theta_{rise} = 3.4 \AA$, and $f_{i\neq j} = 0$, values consistent with previous experiments on the stiffness of DNA \cite{Horowitz1984, Heath1996}. Real structures contain a wide amount of variation from these rest values.

\begin{align}
f_{ij} &= \frac{\partial^2 E}{\partial \theta_i \partial \theta_j}
\end{align}

Determining the constants in equation \ref{eq:harmonic-energy} are obtainable using inverse harmonic analysis, using parameters directly determined using imaged structures. Substituting the observed pairwise parameter averages,

\begin{align}
F^{-1} &= \left\langle \Delta \theta_i \Delta \theta_j \right\rangle = \left\langle \theta_i \theta_j \right\rangle - \left\langle \theta_i \right\rangle \left\langle \theta_j \right\rangle
\end{align} 

and inverting $F^{-1}$, we obtain the stiffness constants between any two neighboring base pairs. Each $F$ is unique to the composition of the basepair steps. For example, $F$ may be different for $A \cdot T$ and $G \cdot C$ step. % strand symmetry

The fraction of closed to open polymers in solution is usually computed in terms of the Jacobson-Stockmayer ($J$) factor \cite{Jacobson1950}. Therefore in the context of LacR-mediated DNA loops, a higher j-factor denotes a higher probability of loops forming and ultimately a higher level of genetic repression. The j-factor is obtained experimentally from sticky-end ligation \cite{Shore1981, Kahn1992}, inferring the degree of looping by tuning the amount of free repressor \textit{in vivo} \cite{Mueller1996, Bintu2005, Bintu2005a, Saiz2005}, and newer single-molecule experiments \cite{Finzi1995, Haeusler2012, Han2009, Johnson2012}.

\section{Computational Modeling of Protein-DNA Interactions}

By assuming the deviations of the step parameters from relaxed DNA are normally distributed, chains of arbitrary length can be quickly built up using normally distributed random numbers \cite{Czapla2006}, a procedure known as Gaussian sampling. Each step parameter is drawn from a normally distributed random number generator with mean and variance derived from a Boltzmann distribution of states from equation \ref{eq:harmonic-energy}. This sampling algorithm is used in tandem with the Alexandrowicz half-chain sampling technique, which generates $\sqrt{N}$ half-chain configurations to sample $N$ configurations. Architectural proteins are placed randomly along the loop dictated by ratio of protein in the cell to the length of the \textit{E. coli} genome. Special care is needed when using the Alexandrowicz algorithm because these proteins may span the joined segments.  To simulate the anchoring conditions imposed by the Lac repressor protein, a fake step is inserted from the last base pair back to the first, to close and form the loop. Candidate configurations are then accepted or rejected based on the degree which they overlap with the first step. This allows us to express the mathematics of these protein-mediated loops in terms of ring closure probabilities.


Calculation of topological values, such as the twist of supercoiling, writhe, and linking number are useful in comparing large groups of structures and determining whether seemingly similar loop types exist in multiple structural states. A ring is defined as the looped, free base pairs, continuing midway through the operator DNA, down through the Gln335 C-$\alpha$ atom on the nearest monomer, through the base of the LacR protein to the other Gln335 C-$\alpha$ atom, and upwards through the other operator and back to the first free base pair \cite{Swigon2006}. In practice, this computation is done via the DNASim C++ package developed by Nicolas Clauvelin in the Olson lab \cite{Clauvelin2012}.

Atomic-level looped structures are exported from simulation in three or more parts; two LacR halves and bound architectural proteins in PDB format and the generated protein-free DNA as a list of step parameters convertible to PDB using the 3DNA command "rebuild". I have developed several scripts using both 3DNA \cite{Lu2003,Lu2008} and PyMol \cite{DeLano2002} align, orient, and style individual structures.

\section{Lac Repressor-mediated DNA Looping}

\subsection{Introduction}
% What have people found before me from experiments?

\subsection{LacR-mediated looping generally easier than minicircle cyclization}
% What have we found thus far about the impact of LacR over minicircles?
The probability and $J$ factor of cyclization is about 4-5 orders of magnitude higher for loops of the same length formed with LacR compared to protein-free minicircles. This result is not surprising since the energetic requirements for minicircles are generally higher due to the need for increased bending. A covalently closed minicircle requires nearly  However, the presence of LacR disrupts the pattern of peaks and troughs in $J(N)$, adjusting by about half of a period.

\begin{figure}[t!]
\centering
\includegraphics[scale=.45]{j_from_HU.png}
\caption{\small Protein-free, covalently closed DNA minicircles are usually the least likely to form at all chain lengths. LacR-mediated, HU-free loops are more likely than minicircles because the intervening DNA does not need to be as bent. LacR-mediated loops in the presence of HU have nearly constant levels of $J(N)$. All three categories of looped structures form with different likelihoods and oscillate $J(N)$ out of phase. Figure taken from \cite{Czapla2013}}
\label{fig:j_from_hu}
\end{figure} 

\subsection{Effect from flexible LacR}
% What is the basis experimentally for thinking this occurs?
The structure and behavior of LacR itself is a key constituent in looping. As shown in Figure \ref{fig:LacOperonStructure}, LacR is a V-shaped dimer of dimers with two flexible DNA-binding headpiece regions \cite{Lewis1996, Bell2000}. There is significant evidence that LacR can deform itself by spreading open its two arms anywhere between $ \Delta \alpha = $ 30 and 180 degrees, and is the subject of debate whether this action affects looping probabilities \textit{in vivo} \cite{Morgan2005, Rutkauskas2009, Hirsh2011, Towles2009, Villa2005}. There are four possible orientations DNA may take while bound to the Lac repressor as displayed in Figure \ref{fig:Orientations}. The two parallel (P1 and P2) and two anti-parallel (A1 and A2) orientations are determined based on the 5' to 3' directionality of the DNA; A1 and A2 loops frequently have similar properties and P2 loops are usually the least likely to form.

We have predicted that incorporating a flexible Lac repressor in simulation will dampen the oscillations of $J(N)$ compared to similar fixed repressor-mediated loops. This can explain how gene-expression studies have found less variation in $J$ than our previous fixed LacR simulations suggest. Flexibility in LacR will increase $J$, but the effect from flexible LacR is not as significant as the effect from HU. A change in the phase of oscillations occurs due to changes in twist from the opening of the repressor. Peaks in $J$ correspond to low $ \Delta \alpha$, since the LacR protein does not need to accommodate out of phase DNA. Our current model only posits that LacR is flexible about an axis through the base of LacR V, there is all-atom molecular dynamics experiments showing the DNA binding regions of LacR may rotate and possibly affect $J$ \cite{Villa2005}.

\subsection{Effect from loop length}
One of the most fundamental factors influencing the repressing strength of LacR is the spacing between the DNA it must bind. DNA segments shorter than the persistence length $(\approx 150bp)$ are stiff and cannot usually bend enough to form a loop, whereas much longer segments have too much configurational freedom and often their ends will not come near enough to loop. Figure \ref{fig:j_from_hu} shows that the presence of the LacR increases the ease which cyclization occurs compared to covalently closed, non-LacR bound, minicircle DNA. This is a result of the extended anchoring conditions of LacR permitting less constrained looping configurations. A secondary length-dependent factor is imposed by the helical nature of DNA requiring a natural level of twisting between successive steps. Figure \ref{fig:j_from_hu} shows an oscillatory dependence of $J$ on length ($J(N)$) with periods of 10-12bp, gradually becoming less pronounced with increasing length as DNA has more freedom to close into a loop. The chain-length dependence of the oscillations mimics those found in gene-expression experiments but they overestimate the amplitude of $J(N)$ \cite{Becker2007,Becker2005,Bond2010}.

\subsection{Effect from the HU protein}

The histone-like, heat unstable architectural dimer protein HU binds to DNA in a non-sequence specific manner, maintains negative supercoiling density, and induces large bends along the chain.\cite{Berger2009, Becker2007, Rouviere-Yaniv1979}. HU has been implicated in decreasing persistence length in high concentrations and conversely increasing persistence length when present in low concentrations \cite{Nir2011}. Approximately $30,000$ HU dimers are present in the $4.6 \times 10^6$ bp genome of an \textit{E. coli} cell during its exponential growth phase, or 1 HU per 150 bp \cite{AliAzam1999,Blattner1997}.

As demonstrated in Figure \ref{fig:j_from_hu}, we have shown that the random incorporation of HU at a concentration of 1 per 150bp increases $J$ by 5 to 7 orders of magnitude over HU-free LacR mediated loops \cite{Czapla2013}. HU reduces the energy of the looped system by about $30 k T$ compared to non-HU bound structures for 92bp loops, allowing for much more favorable looping.  HU alters the period of oscillations in $J$, hinting that HU may significantly modify the structure of loops bound with HU compared to unbound loops. Also, HU shows much less dependence on chain length compared to non-HU bound species outside of the oscillations dependent on the helical repeat of DNA.

As expected, our simulations show that a decrease in HU binding probability, as a result of a decrease in available HU in the nucleus, will correspond to decreasing $J$ factors and slightly restoring the peaks of the oscillations in $J$ to the HU-free loops. Decreasing HU to a concentration corresponding to 1/500bp steps and allowing for some motion in LacR's arms will result in a $J$ profile which closely matches the values observed in wild-type cells, shown in Figure \ref{fig:HUat500}. An explanation for this phenomena is measured levels of HU might be higher than the level actually bound to DNA at any one time, despite successful loops forming with on average more HU than the binding probability suggests.

Despite the HU protein's ability to bind in a near random fashion along the looped DNA, we have identified certain locations and spacings along wild-type 92bp loops where the presence of HU seems to enhance loop formation, and thus HU tends to build up at these locations, shown in Figure \ref{fig:HU_locations}. One of the most common patterns for HU binding is placing 2 HUs with each only a few bp from the operators on parallel loops like in Figure \ref{fig:LacOperonStructureWithHU}. A fixed LacR will nearly require HUs spread out by about 35bp on P1 loops and 50bp on P2 loops, a flexible LacR will disrupt this pattern and allow for at least two peaks in spacing for parallel loops. For loops of an integral number of helical repeats long, HU tends to bind at the apex of anti-parallel loops so the loop can retain a generally smooth profile and will increase the variance in writhing number. Interestingly, for loops of a half-integral number of helical repeats long, non-HU bound parallel loops dominate over all HU-bound loops. Considering 401bp loops, the length between the $O_1 \ldots O_2$ operators, the HU positions appear completely random and the addition of HU to these systems has little effect on $J$.

\subsection{Effect from operator sequences}

The structure, sequence, and locations of the operator sites of the Lac operon (shown in Figure \ref{fig:LacOperonZoomedIn}) factor heavily on the repressing ability of the system. The principal $O_1$ operator, located within the $\beta$-galactosidase promoter, strongly binds LacR. Two weaker auxiliary operators exist 92 base pairs upstream ($O_3$) and 401 base pairs downstream ($O_2$) and may also bind LacR. NMR structures of these three operators show the structural similarity of the $O_3$ operator states to vary much more compared to the $O_1$ and $O_2$ operators, as shown in Figure \ref{fig:LacOperonZoomedIn}. The $O_3$ operator also binds DNA weaker than any of the other operators, a result of a reduction in contact points between operator and LacR on half of its headpiece \cite{Romanuka2009}. The synthetic, palindromic $O_{id}$ operator binds operon DNA about ten times tighter than $O_1$ and has been a useful aid in studying the structure of LacR-mediated loops \cite{Sadler1983}. Loops formed between the $O_1 \ldots O_3$ operators are of particular interest due to their short length and biological significance of blocking key transcription sites for the Lac genes. 

In most of our studies we assumed the two operator regions of the Lac operon to be identical and palindromic, using the step parameters derived from the $O_{id}$ operator. New NMR structures of the $O_{sym}$, $O_1$, $O_2$, and $O_3$ regions show a wide range of possible end conditions within each pairing of operators, especially for $O_1 \ldots O_3$ loops (see Figure \ref{fig:OperatorsSuperimposed}) \cite{Spronk1999, Romanuka2009}. Interchanging $O_{sym}$ to $O_2$ will also radically modify the oscillations of $J(N)$ \cite{Mueller1996}. In wild-type length loops formed between $O_1 \ldots O_3$ the angle between anchoring base pairs may fluctuate as much as 40-90 degrees, affecting $J$ by a factor of 2 [cite biopolymers paper]. To study the effect these operators have on looping, we began by superimposing the NMR structures onto a high-resolution composite Lac repressor structure \cite{Swigon2006}, aligning to selected common atoms in the headpiece and deleting extraneous base pairs. By using the 3DNA software developed in the Olson lab \cite{Lu2003,Lu2008}, we could extract the boundary conditions suitable for forming a loop in all four orientations.This produced 800 $O_3 \ldots O_1$ loops, 1600 $O_1 \ldots O_2$ loops and 1936 $O_{sym} \ldots O_{sym}$ loops. The simulations reveal in Figure \ref{fig:J_mosaics} that the highly variable $O_1 \ldots O_3$ boundaries could vary $J$ by over two orders of magnitude, whereas most $O_{sym} \ldots O_{sym}$ loops varied over a single order of magnitude, seen in Figure \ref{fig:J_mosaics}. There are still many lingering questions worthy of investigation in these loops. For instance, there appears to be dependencies on the operator spacing's spatial distance and netbend ($\sqrt{\mathrm{roll}^2+\mathrm{twist}^2+\mathrm{tilt}^2}$) on $J$, changes in topological twist due to operator structure, and whether these patterns hold up with a flexible LacR and addition of HU proteins. 

Simulations by other research groups with less stringent adherence to the anchoring conditions \cite{LaPenna2010} and experiments \textit{in vitro} \cite{Wong2008,Tsodikov1999} have postulated the existence of "wrap around" loops where DNA wraps below the LacR, as opposed to the "wrap away" loops in Figures \ref{fig:LacOperonStructureWithHU} and \ref{fig:LacOperonStructure}. We hoped that the greater range of anchoring conditions afforded by these NMR structures would have produced wrap around loops, however, none were generated. Other processes not yet included in our model such as electrostatic interactions and deformability of LacR headpieces may be needed to generate wrap around loops.

\section{Loop Database And Website}

The simulations of LacR mediated DNA loops yield an enormous amount of information useful to other structural biologists and students. I envision an online repository of loops which other scientists can visualize and download, modeled after databases created by past Olson lab members such as Zheng's w3DNA \cite{Zheng2009} and Britton's TwiDDL \cite{Britton2009}. A wealth of statistics could be computed and displayed for the end user from the data generated from my simulations. Individual structures, like those shown in Figures \ref{fig:LacOperonStructureWithHU} and \ref{fig:LacOperonStructure}, would be indexed and searchable by length, LacR type, orientation, architectural proteins bound, and operator structure. Each structure would be displayed with its computed topological parameters, $J$, and links to download the structure in PDB format. A significant proportion of the site would be dedicated to educating non-experts on the phenomena of protein-bound DNA looping with information on the biology of the Lac operon, physics of DNA flexibility and computational methods to simulate these structures. I plan on distributing my simulation code in an "open-source" manner; anyone may download the full simulation code and suggest changes for improvement. The simulation code itself should work towards an architecture of being both extensible to other protein-mediated looping systems and easy to use for other computational scientists.

\section{Further Proposed Research}

The architectural proteins Fis, IHF, Lrp, Hfq, H-NS, Nhp6A, and several more DNA-binding proteins are present in the \textit{E. coli} nucleus at levels similar to HU throughout its lifecycle \cite{AliAzam1999, Oberto2001}. The Fis protein has also been directly implicated in promoting DNA looping \textit{in vitro} \cite{Skoko2006}, but bends DNA at a less dramatic angle ($65 \deg$) \cite{Stella2010}. Preliminary simulations have shown Fis to increase $J$ by about an order of magnitude over architectural protein-free DNA but only bind in LacR-mediated loops 1/100 times less than HU. An avenue of possible research would be to determine how Fis and HU cooperate to form loops and if the presence of Fis disrupts HU's binding patterns.

Recent tethered particle motion experiments by Johnson et. al on LacR bound DNA loops have shown that the sequence of looped DNA base pairs has an appreciable effect on the observed $J$.\cite{Johnson2012}. In all the previous simulations mentioned in this proposal, the DNA was assumed to be ideal B-DNA. There was no allowance for anisotropy, such as the well known roll-tilt coupling revealed in covariance analysis \cite{Olson1998}, or sequence specific effects, such as the bending from long tracts of repeating adenines \cite{MacDonald2001}. Data on the apparent persistence length of the 16 Watson-Crick base pair steps exists and could be integrated into simulation \cite{Geggier2010, Morozov2008}. An interesting exercise would be to update the force constants in equation \ref{eq:harmonic-energy} to better fit the existing persistence length data and try to reproduce the results found by Johnson. The actual DNA sequence in the Lac operon could then be compared with the current idealized sequence to determine what effect sequence plays in looping.

Another interesting avenue of future research is improving on the fundamental energy equation given in equation \ref{eq:harmonic-energy}. Atomic force microscopy (AFM) experiments by Wiggins showed short DNA segments can spontaneously form sharp kinks ($\geq 1.1$ rad) 30 times more often than predicted by the worm-like chain model \cite{Wiggins2006}. This spurred the development of the "linear sub-elastic chain" (LSEC) model where $E \propto | x |$ as opposed to $ E \propto x^2$ in equation \ref{eq:harmonic-energy}. Recent mathematical advances in materials science studying the cracking of strained materials \cite{Charlotte2008} has shown similar energy profiles as the LSEC model, and would modify equation \ref{eq:harmonic-energy} to

\begin{align}
E &= E_0 + \epsilon^2 \pm l \left( \nabla \epsilon \right)^2 = E_0 + \frac{1}{2} \Delta x^T F \Delta x + \frac{1}{\sqrt{12}}\left( \frac{x^{i+1} - x^{i-1}}{l} \right)
\end{align}

In this scheme, step parameters in this scheme are no longer independently distributed, an assumption critical for the Gaussian Sampling algorithm, so the Metropolis Monte Carlo sampling technique would be required. Metropolis Monte Carlo sampling would also allow modeling of electrostatic interactions.

\section{Conclusion}

The question of what factors are most important in genetic regulation by protein-mediated DNA looping is still an unresolved question and is likely the composition of several structural and physical dependencies. It is still unclear how much each of the variables, such as the length and sequence of looped DNA, presence of architectural proteins like HU, intrinsic flexibility of the LacR protein, assumed physical model of DNA, and structure of anchoring operator segments, all affect looping and whether their effects are simply additive. We already have strong reason to believe that the collective behavior is "greater than the sum of its parts" \cite{Czapla2013}. The goal of my thesis will be to work towards a more complete description of DNA looping through the model Lac operon system.

\begin{figure}[h]
\centering
\includegraphics[scale=.3]{Orientations.png}
\caption{\small DNA bound to the two LacR operators has four possible orientations governed by the $5' \rightarrow 3'$ directionality of the bound DNA. The DNA can be either parallel (P) or anti-parallel (A), and  the first operator (closest to the $5'$ end, or $O_3$ for $O_3 \ldots O_1$ loops) points the DNA inside (1) or outside (2). Image taken from \cite{Czapla2013}. }
\label{fig:Orientations}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=1]{HUat500.png}
\caption{\small $J(N)$ of loops bound by flexible and fixed LacR can be combined by penalizing flexible LacR opening by $1.8 kT$, a conservative estimate of the free energy cost deduced from the surface area of the contact interface \cite{Swigon2006}. At a level of 1 HU protein every 500 base pairs, the weighted $J$ closely mirrors results from gene-expression studies with HU (WT) \cite{Becker2005}. In the absence of HU, the computed $J$ is several orders of magnitude less and out of phase with the gene-repression data. $J(N)$ measured in HU depleted cells ($\Delta \mathrm{HU}$) is consistent with data obtained in HU-depleted, tethered particle motion experiments \cite{Czapla2013, Han2009}. Image taken from \cite{Olson2012}. }
\label{fig:HUat500}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=.4]{OperatorsSuperimposed.png}
\caption{\small Images of the 10 $O_3$, 20 $O_1$, 11 $O_{sym}$, and 20 $O_2$ operators superimposed on their central base pair taken from [Biopolymers REF]. The $O_3$ operator shows the widest variation in structure.}
\label{fig:OperatorsSuperimposed}
\end{figure} 

\onecolumn

\begin{figure}[h]
\centering
\includegraphics[scale=1]{J_mosaics.png}
\caption{\small Mosaics of $\log J$ values of $O_{sym} \ldots O_{sym}$ and $O_3 \ldots O_1$ loops taken from [Biopolymers REF]. $10^{15}$ configurations for every of the 121 combinations of $O_{sym} \ldots O_{sym}$ and 200 $O_3 \ldots O_1$ boundaries are generated using the Gaussian sampling method outlined previously, and separated by orientation (not enough P2 loops are generated to be significant). Notice that fluctuations in the end conditions $O_3 \ldots O_1$ can vary $J$ by a factor of $\pm 2$ orders of magnitude, while the fluctuations in $O_{sym} \ldots O_{sym}$ end conditions vary $J$ over a single order of magnitude. Image taken from [Biopolymers REF]}
\label{fig:J_mosaics}
\end{figure} 

\begin{figure}[h]
\centering
\includegraphics[scale=1]{HU_locations.png}
\caption{\small HU tends to bind in distinctive locations on Lac loops depending on loop length, LacR geometry, and DNA orientation (A1, A2, P1, P2). P1 and P2 show the most striking patterns of HU placement. The parallel loops show less disruption in HU patterning due to lower average opening angle $ \Delta \alpha \approx 16$ compared to anti-parallel loops $\Delta \alpha \approx 30$. The shaded background areas correspond to the CAP (red, see figure \ref{fig:LacOperonZoomedIn}), -35 (green) and -10 (blue) sites.}
\label{fig:HU_locations}
\end{figure} 

\begin{figure}[h]
\centering
\includegraphics[scale=1]{combo_fig.png}
\caption{\small $J$ as a function of looped DNA length in base pairs ($J(N)$) is plotted in the top row and the proportion of loops at length $N$ with one (green), two (red), or three (blue) HU is plotted in the bottom row. Flexible, deformable LacR is on the left column and fixed LacR is on the right. Flexible LacR tends to smooth out the peaks and valleys in $J(N)$ and increases $J(N)$ on average. Fixed loops tend to be dominated by two HU when $J(N)$ is increasing, while flexible loops tend to be dominated by two HU near local minimums in $J(N)$. Taken together, these plots show how the complex interactions of the Lac operon interact together in a non-additive fashion.}
\label{fig:combo_fig}
\end{figure} 

% Bibliography
{
	\footnotesize
	\bibliographystyle{abbrv}
	\bibliography{/Users/grosner/Dropbox/bibdb}
}

\end{document}